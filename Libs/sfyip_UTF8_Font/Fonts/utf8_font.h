#ifndef _UTF8_FONT_H_
#define _UTF8_FONT_H_

#include <stdint.h>
#include <stdbool.h>


//#define CONFIG_FONT_FIXED_WIDTH_HEIGHT 	    0
#define CONFIG_FONT_ENC                     3 // 0: raw, 1: rawbb, 3: lvgl
//# raw: direct dump the pixels inside the margin area
//# rawbb: direct dump the pixel inside margin area (bounding box)
//# lvgl: use lvgl font compression (modified i3bn)
//#define CONFIG_FONT_MARGIN                  0
// define the maximum size of margin top, margin bottom, margin left, margin width
#define FONT_MARGIN_DATABIT_SIZE            8

//=================================================================
typedef uint32_t utf8_t;
extern const utf8_t utf8_map[];

typedef struct
{
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT == 0u)
    // adapt size
    uint8_t width;
    uint8_t height;
#endif

#if (CONFIG_FONT_ENC >= 1u && CONFIG_FONT_ENC <= 3u)
    // calc_margin
    uint8_t margin_top      :FONT_MARGIN_DATABIT_SIZE;
    uint8_t margin_bottom   :FONT_MARGIN_DATABIT_SIZE;
    uint8_t margin_left     :FONT_MARGIN_DATABIT_SIZE;
    uint8_t margin_right    :FONT_MARGIN_DATABIT_SIZE;
#endif

    uint16_t bmp_index;

#if (CONFIG_FONT_ENC >= 2u && CONFIG_FONT_ENC <= 3u)
    uint8_t size;
#endif
}font_symbol_t;

//=================================================================


typedef bool (*fnt_lookup_fp)(utf8_t c, font_symbol_t *sym);

typedef struct
{
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u) 
    uint8_t width;
    uint8_t height;
#else
    uint8_t default_width;
    uint8_t default_height;
#endif

    const uint8_t *bmp_base;

    fnt_lookup_fp lookup;
}font_t;

//=================================================================
extern font_t uvnanhhai15;
extern font_t uvnanhhai26;
extern font_t roboto_regular54;
extern font_t roboto_regular14;
extern font_t roboto_regular40;

int binary_search(utf8_t x);
int linear_search(utf8_t x);
#endif
