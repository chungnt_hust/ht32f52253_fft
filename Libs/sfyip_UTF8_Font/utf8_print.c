#include "utf8_print.h"
#include "utf8_decode.h"
//=========================================================================

static rect_t utf8_print_screen_bond;
static point_t utf8_print_cur;
static setPixcel_func_t utf8_etpx;
//=========================================================================

#define ROW_CLERANCE_SIZE   5

static void _draw_pixel(uint16_t x, uint16_t y, uint16_t color);
static void _set_bound(uint16_t startx, uint16_t starty, uint16_t endx, uint16_t endy);
static void _write_gram(uint16_t color, uint8_t color_pixel_enable);
#if(CONFIG_FONT_ENC == 0u)
static void font_render_engine_raw(const font_t *fnt, const font_symbol_t *sym, uint16_t color);
#elif(CONFIG_FONT_ENC == 1u)
static void font_render_engine_rawbb(const font_t *fnt, const font_symbol_t *sym, uint16_t color);
#elif(CONFIG_FONT_ENC == 3u)
static void font_render_engine_lvgl(const font_t *fnt, const font_symbol_t *sym, uint16_t color);
#endif
//=========================================================================
void utf8_print_RegDrawPixelFunction(setPixcel_func_t func)
{
    utf8_etpx = func;
}

//=========================================================================
void utf8_print_charNoBackGnd(uint16_t x, uint16_t y, const font_t *fnt, utf8_t c, uint16_t color)
{
    font_symbol_t sym;

    if(!fnt->lookup(c, &sym))
    {
        return;
    }

#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u)
    uint8_t font_width = fnt->width;
    uint8_t font_height = fnt->height;
#else
    uint8_t font_width = sym.width;
    uint8_t font_height = sym.height;
#endif

    _set_bound(x, y, x+font_width-1, y+font_height-1);

#if (CONFIG_FONT_ENC == 0u)
    font_render_engine_raw(fnt, &sym, color);
#elif(CONFIG_FONT_ENC == 1u)
    font_render_engine_rawbb(fnt, &sym, color);
#elif(CONFIG_FONT_ENC == 3u)
    font_render_engine_lvgl(fnt, &sym, color);
#else
    #error "Unsupported ENCODING_METHOD"
#endif

//    _set_bound(0, 0, 320-1, 240-1);
}

//=========================================================================
void utf8_print_stringNoBackGnd(uint16_t x, uint16_t y, const font_t *fnt, const char *s, uint16_t color)
{
    uint16_t orgx = x;
    utf8_t c;
    while((c = utf8_getchar(s)) != '\0')
    {
        if(c == '\r')
        {
            // no operation
        }
        else if(c == '\n')
        {
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u)
            y += (fnt->height + ROW_CLERANCE_SIZE);
#else
            y += (fnt->default_height + ROW_CLERANCE_SIZE);
#endif
            x = orgx;
        }
        else if(c == ' ')
        {
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u)
            x += fnt->width;
#else
            x += fnt->default_width;
#endif
        }
        else
        {
            utf8_print_charNoBackGnd(x, y, fnt, c, color);
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u)
            x += fnt->width;
#else
            font_symbol_t sym;
            if(!fnt->lookup(c, &sym))
            {
                x += fnt->default_width;
            }
            else
            {
                x += sym.width;
            }
#endif
        }
        s += utf8_charlen(c);
    }
}

//=========================================================================
static void _draw_pixel(uint16_t x, uint16_t y, uint16_t color)
{
    
}

//=========================================================================
static void _set_bound(uint16_t startx, uint16_t starty, uint16_t endx, uint16_t endy)
{
    utf8_print_screen_bond.x0 = startx;
    utf8_print_screen_bond.y0 = starty;
    utf8_print_screen_bond.x1 = endx;
    utf8_print_screen_bond.y1 = endy;

    utf8_print_cur.x = startx;
    utf8_print_cur.y = starty;
}

//=========================================================================
static void _write_gram(uint16_t color, uint8_t color_pixel_enable)
{
    if(color_pixel_enable == 1) _draw_pixel(utf8_print_cur.x, utf8_print_cur.y, color);

    ++utf8_print_cur.x;
    if(utf8_print_cur.x > utf8_print_screen_bond.x1)
    {
        utf8_print_cur.x = utf8_print_screen_bond.x0;
        ++utf8_print_cur.y;
    }

    if(utf8_print_cur.y > utf8_print_screen_bond.y1)
    {
        utf8_print_cur.x = utf8_print_screen_bond.x0;
        utf8_print_cur.y = utf8_print_screen_bond.y0;
    }
}

//=========================================================================
#if (CONFIG_FONT_MARGIN == 0u && CONFIG_FONT_ENC == 0u)
// encoding method: raw
static void font_render_engine_raw(const font_t *fnt, const font_symbol_t *sym, uint16_t color)
{
    uint16_t i = 0;
    uint8_t j = 0;
    
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u)
    uint16_t area = fnt->width * fnt->height;
#else
    uint16_t area = sym->width * sym->height;
#endif

    const uint8_t *bmp = (const uint8_t*)(fnt->bmp_base + sym->bmp_index);

    while(area--)
    {
        uint8_t color_pixel_enable = (bmp[i] >> j) & 0x01;
        _write_gram(color, color_pixel_enable);

        if(j == 7)
        {
            ++i;
            j = 0;
        }
        else
        {
            ++j;
        }
    }
}
#endif

#if (CONFIG_FONT_ENC == 1u)
// encoding method: rawbb
static void font_render_engine_rawbb(const font_t *fnt, const font_symbol_t *sym, uint16_t color)
{
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u)
    uint8_t font_width = fnt->width;
    uint8_t font_height = fnt->height;
#else
    uint8_t font_width = sym->width;
    uint8_t font_height = sym->height;
#endif

    uint8_t top = sym->margin_top;
    uint8_t bottom = font_height - sym->margin_bottom-1;
    uint8_t left = sym->margin_left;
    uint8_t right = font_width - sym->margin_right-1;
    
    uint16_t bi = sym->bmp_index;

    uint8_t h, w;
    uint8_t i = 0;

    for(h=0; h<font_height; h++)
    {
        for(w=0; w<font_width; w++)
        {
            if(w < left || w > right || h < top || h > bottom)
            {
                // debug: _write_gram(LCD_BLUE_COLOR);
                _write_gram(color, 0);
            }
            else
            {
                uint8_t color_pixel_enable = (fnt->bmp_base[bi] >> i) & 0x01;
                _write_gram(color, color_pixel_enable);

                if(i==7)
                {
                    i = 0;
                    ++bi;
                }
                else
                {
                    ++i;
                }
            }
        }
    }
}
#endif

#if (CONFIG_FONT_ENC == 3u)
// encoding method: lvgl
typedef enum {
    RLE_STATE_SINGLE = 0,
    RLE_STATE_REPEATE,
    RLE_STATE_COUNTER,
} rle_state_t;

static uint32_t rle_rdp;
static const uint8_t * rle_in;
static uint8_t rle_bpp;
static uint8_t rle_prev_v;
static uint8_t rle_cnt;
static rle_state_t rle_state;

static inline void rle_init(const uint8_t * in,  uint8_t bpp);
static inline uint8_t rle_next(void);

static void font_render_engine_lvgl(const font_t *fnt, const font_symbol_t *sym, uint16_t color)
{
#if (CONFIG_FONT_FIXED_WIDTH_HEIGHT > 0u)
    uint8_t font_width = fnt->width;
    uint8_t font_height = fnt->height;
#else
    uint8_t font_width = sym->width;
    uint8_t font_height = sym->height;
#endif

    uint8_t top = sym->margin_top;
    uint8_t bottom = font_height - sym->margin_bottom-1;
    uint8_t left = sym->margin_left;
    uint8_t right = font_width - sym->margin_right-1;

    uint16_t bi = sym->bmp_index;

    uint8_t h, w;
    uint8_t i=0;

    rle_init(&fnt->bmp_base[bi], 1); // 1: 1 bit per pixel

    for(h=0; h<font_height; h++)
    {
        for(w=0; w<font_width; w++)
        {
            if(w < left || w > right || h < top || h > bottom)
            {
                // debug: _write_gram(LCD_BLUE_COLOR);
                _write_gram(color, 0);
            }
            else
            {

                uint8_t b = rle_next() & 0x01;
                _write_gram(color, b);
            }
        }
    }
}

/**
 * Read bits from an input buffer. The read can cross byte boundary.
 * @param in the input buffer to read from.
 * @param bit_pos index of the first bit to read.
 * @param len number of bits to read (must be <= 8).
 * @return the read bits
 */
static inline uint8_t get_bits(const uint8_t * in, uint32_t bit_pos, uint8_t len)
{
    uint8_t bit_mask;
    switch(len) {
        case 1:
            bit_mask = 0x1;
            break;
        case 2:
            bit_mask = 0x3;
            break;
        case 3:
            bit_mask = 0x7;
            break;
        case 4:
            bit_mask = 0xF;
            break;
        case 8:
            bit_mask = 0xFF;
            break;
        default:
            bit_mask = (uint16_t)((uint16_t) 1 << len) - 1;
    }

    uint32_t byte_pos = bit_pos >> 3;
    bit_pos = bit_pos & 0x7;

    if(bit_pos + len >= 8) {
        uint16_t in16 = (in[byte_pos] << 8) + in[byte_pos + 1];
        return (in16 >> (16 - bit_pos - len)) & bit_mask;
    }
    else {
        return (in[byte_pos] >> (8 - bit_pos - len)) & bit_mask;
    }
}

static inline void rle_init(const uint8_t * in,  uint8_t bpp)
{
    rle_in = in;
    rle_bpp = bpp;
    rle_state = RLE_STATE_SINGLE;
    rle_rdp = 0;
    rle_prev_v = 0;
    rle_cnt = 0;
}

static inline uint8_t rle_next(void)
{
    uint8_t v = 0;
    uint8_t ret = 0;

    if(rle_state == RLE_STATE_SINGLE) {
        ret = get_bits(rle_in, rle_rdp, rle_bpp);
        if(rle_rdp != 0 && rle_prev_v == ret) {
            rle_cnt = 0;
            rle_state = RLE_STATE_REPEATE;
        }

        rle_prev_v = ret;
        rle_rdp += rle_bpp;
    }
    else if(rle_state == RLE_STATE_REPEATE) {
        v = get_bits(rle_in, rle_rdp, 1);
        rle_cnt++;
        rle_rdp += 1;
        if(v == 1) {
            ret = rle_prev_v;
            if(rle_cnt == 11) {
                rle_cnt = get_bits(rle_in, rle_rdp, 6);
                rle_rdp += 6;
                if(rle_cnt != 0) {
                    rle_state = RLE_STATE_COUNTER;
                }
                else {
                    ret = get_bits(rle_in, rle_rdp, rle_bpp);
                    rle_prev_v = ret;
                    rle_rdp += rle_bpp;
                    rle_state = RLE_STATE_SINGLE;
                }
            }
        }
        else {
            ret = get_bits(rle_in, rle_rdp, rle_bpp);
            rle_prev_v = ret;
            rle_rdp += rle_bpp;
            rle_state = RLE_STATE_SINGLE;
        }

    }
    else if(rle_state == RLE_STATE_COUNTER) {
        ret = rle_prev_v;
        rle_cnt--;
        if(rle_cnt == 0) {
            ret = get_bits(rle_in, rle_rdp, rle_bpp);
            rle_prev_v = ret;
            rle_rdp += rle_bpp;
            rle_state = RLE_STATE_SINGLE;
        }
    }

    return ret;
}
#endif

