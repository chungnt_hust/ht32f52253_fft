#ifndef _LCDSIM_H_
#define _LCDSIM_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "utf8_font.h"

//=========================================================================
#define RGB565_TO_R8(color)         (((color) & 0xF800) >> 8)
#define RGB565_TO_G8(color)         (((color) & 0x07E0) >> 3)
#define RGB565_TO_B8(color)         (((color) & 0x001F) << 3)

#define RGB888_TO_RGB565(r,g,b)     (((((lcd_color_t)r) & 0b11111000) << 8) | ((((lcd_color_t)g) & 0b11111100) << 3) | (((lcd_color_t)b) >> 3))

//=========================================================================

#ifndef MIN
    #define MIN(a,b)(a<b?a:b)
#endif

#ifndef MAX
    #define MAX(a,b)(a>b?a:b)
#endif

//=========================================================================
typedef struct
{
    uint32_t x;
    uint32_t y;
}point_t;

typedef struct
{
    uint32_t x0;
    uint32_t y0;
    uint32_t x1;
    uint32_t y1;
}rect_t;

typedef void (*setPixcel_func_t)(uint16_t x, uint16_t y, uint32_t color);
//=========================================================================
void utf8_print_RegDrawPixelFunction(setPixcel_func_t func);
void utf8_print_charNoBackGnd(uint16_t x, uint16_t y, const font_t *fnt, utf8_t c, uint16_t color);
void utf8_print_stringNoBackGnd(uint16_t x, uint16_t y, const font_t *fnt, const char *s, uint16_t color);
void utf8_print_charBackGnd(uint16_t x, uint16_t y, const font_t *fnt, utf8_t c, uint16_t color, uint16_t colorGND);
void utf8_print_stringBackGnd(uint16_t x, uint16_t y, const font_t *fnt, const char *s, uint16_t color, uint16_t colorGND);
#endif
