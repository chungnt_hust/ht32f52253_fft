# from PIL import Image

# HEIGHT_MAX = 64
# WIDTH_MAX  = 128
# BIT_COLOR = 8
# MAX_BIT_COLOR = 8
# BIT_COLOR_EXCEPT = 4

# photo = Image.open('E:\STM32xxxx\Gen Graphics Led Matrix/hinh-nen-meo-de-thuong-37.jpg') #your image
# photo = photo.convert('RGB')

# width  = photo.size[0] #define W and H
# height = photo.size[1]

# if width > WIDTH_MAX:
#     width = WIDTH_MAX
# if height > HEIGHT_MAX:
#     height = HEIGHT_MAX    

# maxIn = pow(2, BIT_COLOR) - 1
# maxOut = pow(2, MAX_BIT_COLOR) - 1

# def gammaOut(x):
#     # return (pow(x/maxIn, 2.3)*maxOut + 0.5)
#     z = pow(x/maxIn, 3.5)*maxOut + 0.5
#     return int(z)
    
# gammaR_factor = 2.5   
# def gammaOutR(x):
#     # return (pow(x/maxIn, 2.3)*maxOut + 0.5)
#     z = pow(float(x/maxIn), gammaR_factor)*maxOut + 0.5
#     return int(z)

# gammaG_factor = 2.5
# def gammaOutG(x):
#     # return (pow(x/maxIn, 2.3)*maxOut + 0.5)
#     z = pow(float(x/maxIn), gammaG_factor)*maxOut + 0.5
#     return int(z)

# gammaB_factor = 2.5 
# def gammaOutB(x):
#     # return (pow(x/maxIn, 2.3)*maxOut + 0.5)
#     z = pow(float(x/maxIn), gammaB_factor)*maxOut + 0.5
#     return int(z)

# for y in range(0, height): #each pixel has coordinates
#     row = ""
#     for x in range(0, width):

#         RGB = photo.getpixel((x,y))
#         R,G,B = RGB  #now you can use the RGB value
#         # temR = R
#         # temG = G
#         # temB = B
        
#         temR = gammaOutR(R>>(MAX_BIT_COLOR-BIT_COLOR))
#         temG = gammaOutG(G>>(MAX_BIT_COLOR-BIT_COLOR))
#         temB = gammaOutB(B>>(MAX_BIT_COLOR-BIT_COLOR))

#         print('{',temR,',',temG,',',temB,'},', end='')
#         # print('{',R>>(8-BIT_COLOR),',',G>>(8-BIT_COLOR),',',B>>(8-BIT_COLOR),'},', end='')
#         # print('% 3d' %G,',', end='')
#         if x == (width-1):
#             print(end='\n')
# print("w, h,", width, height)        

# ##############################################
# from PIL import Image

# HEIGHT_MAX = 240
# WIDTH_MAX  = 320
# BIT_COLOR = 4

# photo = Image.open('D:/_Gen_Font_TFT/aw.jpg') #your image
# photo = photo.convert('RGB')

# width  = photo.size[0] #define W and H
# height = photo.size[1]

# if width > WIDTH_MAX:
#     width = WIDTH_MAX
# if height > HEIGHT_MAX:
#     height = HEIGHT_MAX    

# for y in range(0, height): #each pixel has coordinates
#     row = ""
#     for x in range(0, width):

#         RGB = photo.getpixel((x,y))
#         R,G,B = RGB  #now you can use the RGB value

#         # print('{',R>>(8-BIT_COLOR),',',G>>(8-BIT_COLOR),',',B>>(8-BIT_COLOR),'},', end='')
#         # print('% 3d' %G,',', end='')
#         # if x == (width-1):
#         #     print(end='\n')
# print("w, h,", width, height)        

##############################################
from PIL import Image

HEIGHT_MAX = 240
WIDTH_MAX  = 320

file = 'D:/_Gen_Font_TFT/aj0.jpg'
photo = Image.open(file) #your image
photo = photo.convert("1")
# photo.show()

width  = photo.size[0] #define W and H
height = photo.size[1]

if width > WIDTH_MAX:
    width = WIDTH_MAX
if height > HEIGHT_MAX:
    height = HEIGHT_MAX    


# SOBYTE = (int)(width/8) + 1
SOBYTE = 2
BITWIDTH = SOBYTE*8
print("FILE:", '%s' %file)
print("So byte:", SOBYTE, "do rong bit luu du lieu:", BITWIDTH)
print("w, h,", width, height) 
print("/* ? */", end='')
for y in range(0, height): #each pixel has coordinates
    tempData = 0
    row = ""
    for x in range(0, width):

        BW = photo.getpixel((x,y))
        # print('% 4d' %BW,',', end='')
        # if x == (width-1):
        #     print(end='\n')
        BIT = BW/255
        tempData = tempData<<1
        if BIT == 1:
            tempData = tempData | 1
            
    tempData = tempData<<(BITWIDTH-width)
    # print("BYTE:", '0x%x' %tempData)
    for separate in range(SOBYTE-1, -1, -1):
        # print('byte[%d]' %separate, '0x%x' %((tempData>>(8*separate))&0xff))
        print('0x%02x,' %((tempData>>(8*separate))&0xff), end='')
# print("},", end='\n')
