/*********************************************************************************************************//**
 * gptmPWM.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *************************************************************************************************************
 @PWM mode 1
	- During up-counting, channel 1 has an active level when CNTR < CH1CR
	or otherwise has an inactive level.
	- During down-counting, channel 1 has an inactive level when CNTR >
	CH1CR or otherwise has an active level
	
 @PWM mode 2
	- During up-counting, channel 1 has an inactive level when CNTR <
	CH1CR or otherwise has an active level.
	- During down-counting, channel 1 has an active level when CNTR > 
	CH1CR or otherwise has an inactive level.
	
	@ControlN, PolarityN, IdleStateN are Reserved Bits
	
	@Symmetric vs Asymmetric PWM
							   |<-period->|
									    ___        ___        ___        ___        
	Symmetric		_______|   |______|   |______|   |______|   |______
									___        ___        ___        ___       
	Asymmetric  ___|   |______|   |______|   |______|   |______
 ************************************************************************************************************/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "gptmPWM.h"
#include "systemClockCfg.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
#define MAX_CK_AHB 					60000000 //(ClockFreq.HCLK_Freq) // Hz
#define PWM_PRESCALER       1
#define PWM_FRE    					20000    		// Hz
#define PWM_RELOAD_COUNTER  (MAX_CK_AHB/PWM_FRE)
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  Init PWM function.
  * @retval None
  ***********************************************************************************************************/
void gptmPWM_Init(void)
{
  { /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.GT_PWM_IPN	    	= 1;
    CKCUClock.Bit.AFIO             	= 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Configure AFIO mode as TM function                                                                     */
  AFIO_GPxConfig(PWM_CH2_GPIO_ID, PWM_CH2_AFIO_PIN, GT_PWM_AFIO_FUN);


  { /* Time base configuration                                                                              */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below this function.
    */
    TM_TimeBaseInitTypeDef TimeBaseInit;

    TimeBaseInit.Prescaler = PWM_PRESCALER - 1;            	// Timer clock = CK_AHB / 1
    TimeBaseInit.CounterReload = (PWM_RELOAD_COUNTER) - 1;  // CK_AHB/fre
    TimeBaseInit.RepetitionCounter = 0;
    TimeBaseInit.CounterMode = TM_CNT_MODE_UP;
    TimeBaseInit.PSCReloadTime = TM_PSC_RLD_IMMEDIATE;
    TM_TimeBaseInit(GT_PWM_PORT, &TimeBaseInit);
		/* = 0 Counter reload register can be updated immediately
			 = 1 Counter reload register can not be updated until the update event occurs */
    TM_CRRPreloadCmd(GT_PWM_PORT, ENABLE);

    /* Clear Update Event Interrupt flag since the "TM_TimeBaseInit()" writes the UEV1G bit                 */
    TM_ClearFlag(GT_PWM_PORT, TM_FLAG_UEV);
  }

  { /* Channel n output configuration                                                                       */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below this function.
    */
    TM_OutputInitTypeDef OutInit;

    OutInit.OutputMode 				= TM_OM_PWM1;
    OutInit.Control 					= TM_CHCTL_ENABLE;
    OutInit.ControlN 					= TM_CHCTL_DISABLE;
    OutInit.Polarity 					= TM_CHP_NONINVERTED;
    OutInit.PolarityN 				= TM_CHP_NONINVERTED;
    OutInit.IdleState 				= MCTM_OIS_LOW;
    OutInit.IdleStateN 				= MCTM_OIS_HIGH;
    OutInit.Compare 					= 0;
    OutInit.AsymmetricCompare = 0;

    OutInit.Channel 					= PWM_CH2_CH;
    TM_OutputInit(GT_PWM_PORT, &OutInit); // CH2 Start Output as default value
		// Compare Register Preload Enable
    TM_CHCCRPreloadConfig(GT_PWM_PORT, OutInit.Channel, ENABLE);
  }

	#if 0 // enable or disable interrupt
  /* Enable Update Event interrupt                                                                          */
  NVIC_EnableIRQ(GT_PWM_IRQn);
	#endif
	
  #if 1 // Default enable or disable
  TM_Cmd(GT_PWM_PORT, ENABLE);
  #endif
}
 
/*********************************************************************************************************//**
  * @brief  Set PWM Frequency
  * @param  uReload: Reload value of timer)
  * @retval None
  ***********************************************************************************************************/
void gptmPWM_SetFreq(u32 uReload)
{
  TM_SetCounterReload(GT_PWM_PORT, uReload);
}

/*********************************************************************************************************//**
  * @brief  Update PWM Duty
  * @param  TM_CH_n: Specify the TM channel.
  * @param  uCompare: PWM duty (Compare value of timer)
  * @retval None
  ***********************************************************************************************************/
void gptmPWM_UpdateDuty(TM_CH_Enum TM_CH_n, u32 uCompare)
{
  TM_SetCaptureCompare(GT_PWM_PORT, TM_CH_n, uCompare);
}

/*********************************************************************************************************//**
  * @brief Enable or Disable PWM.
  * @param NewState: This parameter can be ENABLE or DISABLE.
  * @retval None
	* @note: if NewState is equal DISABLE, user must set duty to zero first
  ***********************************************************************************************************/
void gptmPWM_Cmd(ControlStatus NewState)
{
	TM_Cmd(GT_PWM_PORT, NewState);
}

/*********************************************************************************************************//**
  * @brief Test PWM.
  * @param None.
  * @retval None
  ***********************************************************************************************************/
void gptmPWM_test(void)
{
	static uint16_t count = 0;
	static uint8_t updown = 0;
	gptmPWM_UpdateDuty(PWM_CH2_CH, count);
	if(updown == 0)
	{
		count += 10;
		if(count >= PWM_RELOAD_COUNTER) {
			count = PWM_RELOAD_COUNTER;
			updown = 1;
		}
	}
	else
	{
		count -= 10;
		if(count <= 15){
			count = 10;
			updown = 0;
		}
	}
}
