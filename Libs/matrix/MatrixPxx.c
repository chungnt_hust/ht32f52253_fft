/*
 * MatrixPxx.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "MatrixPxx.h"
#include "systemClockCfg.h"
#include "font.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
/*
 *  HCLK = 72MHz
 	SysTick->LOAD = 72000;
	SysTick->VAL  = 0;
	<=> 1ms
*/
#define TIME_INT (250) 
#define MAX_CK_AHB 			40000000 //(ClockFreq.HCLK_Freq) // Hz
#define PWM_PRESCALER   1
#define PWM_FRE    			20000    		      // Hz
#define PWM_RELOAD_COUNTER  (MAX_CK_AHB/PWM_PRESCALER/PWM_FRE)

static HT_GPIO_TypeDef *gpioPort[]  = {HT_GPIOB,   HT_GPIOB, 	 HT_GPIOB, 	 HT_GPIOB, 	 HT_GPIOB, 	 HT_GPIOB,	 HT_GPIOB, 	 HT_GPIOB, 	 HT_GPIOB, 	 HT_GPIOB, 	 HT_GPIOB,		HT_GPIOB,		 HT_GPIOA, 	 HT_GPIOA};
static uint32_t valueGpioSetReset[] = {GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2, GPIO_PIN_3, GPIO_PIN_4, GPIO_PIN_5, GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_8, GPIO_PIN_10, GPIO_PIN_11, GPIO_PIN_12, GPIO_PIN_7, GPIO_PIN_6};
static HT_GPIO_TypeDef *gpioDataPort = HT_GPIOB;
static pixel_t Panel[MAX_ROW][MAX_COL]; // data = 1 <=> led on
static uint32_t timeSlice[8] = {1*TIME_INT-1, 2*TIME_INT-1, 4*TIME_INT-1, 8*TIME_INT-1, 16*TIME_INT-1, 32*TIME_INT-1, 64*TIME_INT-1, 128*TIME_INT-1};
static point_2D_t ledMatrix;
static uint16_t brightness = 20;
static uint16_t DataOutput[MAX_COL] = {0};
//static uint16_t counterTime[8] = {15, 31, 63, 127, 255, 128, 256, 512};
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
static __INLINE void shiftData(uint8_t row)
{
	uint8_t col;	
//	DataOutput[MAX_COL-1] |= row;
	for(col = 0; col < MAX_COL; col++)
	{		
		gpioDataPort->DOUTR = ((uint32_t)DataOutput[col]);
		gpioPort[CLK_PIN]->SRR = valueGpioSetReset[CLK_PIN]; // set 1
	}
}

static __INLINE void updateBuffer(uint8_t row, uint8_t pos)
{
	uint8_t col, temRow, shift;
	temRow = row+MAX_ROW_SCAN;
	shift = 1<<pos;
	for(col = 0; col < MAX_COL; col++)
	{
		DataOutput[col] = row;
		if((Panel[row][col].r & (shift)) != 0) 			DataOutput[col] |= 0x0020;
		if((Panel[row][col].g & (shift)) != 0) 			DataOutput[col] |= 0x0040;
		if((Panel[row][col].b & (shift)) != 0) 			DataOutput[col] |= 0x0080;
		if((Panel[temRow][col].r & (shift)) != 0) 	DataOutput[col] |= 0x0100;
		if((Panel[temRow][col].g & (shift)) != 0) 	DataOutput[col] |= 0x0400;
		if((Panel[temRow][col].b & (shift)) != 0) 	DataOutput[col] |= 0x0800;
//		((Panel[row][col].r & (shift)) != 0) ? ();
	}
}
	  
/* system tick interrupt callback */
void SysTick_Handler(void)
{
	Delay_Handler();
	
	static volatile uint8_t row = 0;
	static volatile uint8_t bitPos = 0;

	HT_GPTM0->CH2CCR = 0;
	gpioPort[LAT_PIN]->RR = (uint32_t)valueGpioSetReset[LAT_PIN];  // reset 0
	shiftData(row);
	gpioPort[LAT_PIN]->SRR = (uint32_t)valueGpioSetReset[LAT_PIN]; // set 1

	SysTick->LOAD = timeSlice[bitPos];
	SysTick->VAL  = 0;
	bitPos++;
	if(bitPos == MAX_BIT_POS)
	{
		bitPos = 0;
		row++;
		if(row == MAX_ROW_SCAN) row = 0;
	}
	updateBuffer(row, bitPos);
	HT_GPTM0->CH2CCR = brightness;
}

static void gotoXY(uint8_t row, uint8_t col)
{
	ledMatrix.x = col;
	ledMatrix.y = row;
}

static void setColor(uint8_t r, uint8_t g, uint8_t b)
{
	Panel[ledMatrix.y][ledMatrix.x].r = r;
	Panel[ledMatrix.y][ledMatrix.x].g = g;
	Panel[ledMatrix.y][ledMatrix.x].b = b;
	
//	Panel[ledMatrix.y][ledMatrix.x].t = 0;
}

void MATRIX_Init(void)
{
  { /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.GPTM0	    					= 1;
	  CKCUClock.Bit.PA          				= 1;
		CKCUClock.Bit.PB          				= 1;
	  CKCUClock.Bit.PC          				= 1;
    CKCUClock.Bit.AFIO             	  = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* GPIO Output config PB0 ~ PB8, PB10~PB12 */ 
	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_0, AFIO_FUN_GPIO);
  GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_0, GPIO_PR_UP);
  GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_0, GPIO_DIR_OUT);
	
	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_1, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_1, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_1, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_2, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_2, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_2, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_3, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_3, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_3, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_4, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_4, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_4, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_5, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_5, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_5, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_6, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_6, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_6, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_7, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_7, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_7, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_8, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_8, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_8, GPIO_DIR_OUT);
	
	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_10, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_10, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_10, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_11, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_11, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_11, GPIO_DIR_OUT);

	AFIO_GPxConfig(GPIO_PB, AFIO_PIN_12, AFIO_FUN_GPIO);
	GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_12, GPIO_PR_UP);
	GPIO_DirectionConfig(HT_GPIOB, GPIO_PIN_12, GPIO_DIR_OUT);
//  AFIO_GPxConfig(GPIO_PB, 0x0FFF, AFIO_FUN_GPIO);
//  GPIO_PullResistorConfig(HT_GPIOB, 0x0FFF, GPIO_PR_UP);
//  GPIO_DirectionConfig(HT_GPIOB, 0x0FFF, GPIO_DIR_OUT);
//  /* PC5: led debug */
//  AFIO_GPxConfig(GPIO_PC, AFIO_PIN_5, AFIO_FUN_GPIO);
//  GPIO_PullResistorConfig(HT_GPIOC, GPIO_PIN_5, GPIO_PR_UP);
//  GPIO_DirectionConfig(HT_GPIOC, GPIO_PIN_5, GPIO_DIR_OUT);
  
 
  /* Configure AFIO mode as TM function PA6   : PWM                                                             */
  AFIO_GPxConfig(GPIO_PA, AFIO_PIN_6, AFIO_FUN_GPTM0);


	/* Time base configuration       */
	{
		/* !!! NOTICE !!!
			 Notice that the local variable (structure) did not have an initial value.
			 Please confirm that there are no missing members in the parameter settings below this function.
		*/
		TM_TimeBaseInitTypeDef TimeBaseInit;

		TimeBaseInit.Prescaler = PWM_PRESCALER - 1;            	// Timer clock = CK_AHB / 1
		TimeBaseInit.CounterReload = (PWM_RELOAD_COUNTER) - 1;  // CK_AHB/fre
		TimeBaseInit.RepetitionCounter = 0;
		TimeBaseInit.CounterMode = TM_CNT_MODE_UP;
		TimeBaseInit.PSCReloadTime = TM_PSC_RLD_IMMEDIATE;
		TM_TimeBaseInit(HT_GPTM0, &TimeBaseInit);
		/* = 0 Counter reload register can be updated immediately
			 = 1 Counter reload register can not be updated until the update event occurs */
		TM_CRRPreloadCmd(HT_GPTM0, DISABLE);

		/* Clear Update Event Interrupt flag since the "TM_TimeBaseInit()" writes the UEV1G bit                 */
		TM_ClearFlag(HT_GPTM0, TM_FLAG_UEV);
		/* End Time base configuration                                                                          */
	}
	
		/* Channel n output configuration                                                                       */
		/* !!! NOTICE !!!
			 Notice that the local variable (structure) did not have an initial value.
			 Please confirm that there are no missing members in the parameter settings below this function.
		*/
	{
		TM_OutputInitTypeDef OutInit;

		OutInit.OutputMode 				= TM_OM_PWM1;
		OutInit.Control 				  = TM_CHCTL_ENABLE;
		OutInit.ControlN 				  = TM_CHCTL_DISABLE;
		OutInit.Polarity 				  = TM_CHP_INVERTED;
		OutInit.PolarityN 				= TM_CHP_INVERTED;
		OutInit.IdleState 				= MCTM_OIS_HIGH;
		OutInit.IdleStateN 				= MCTM_OIS_HIGH;
		OutInit.Compare 				  = brightness;
		OutInit.AsymmetricCompare = 0;

		OutInit.Channel 				= TM_CH_2;
		TM_OutputInit(HT_GPTM0, &OutInit); // CH0 Start Output as default value
		// Compare Register Preload Enable
		TM_CHCCRPreloadConfig(HT_GPTM0, OutInit.Channel, ENABLE);
		/* End Channel n output configuration                                                                   */
	}
  TM_Cmd(HT_GPTM0, ENABLE);
	
	/* SYSTICK configuration */
  SYSTICK_ClockSourceConfig(SYSTICK_SRC_STCLK);      			// Default : CK_AHB/8
  SYSTICK_SetReloadValue(40000000 / 8 / 1000); 						// (CK_AHB/8/1000) = 1ms on chip
  SYSTICK_IntConfig(ENABLE);                          		// Enable SYSTICK Interrupt
  SYSTICK_CounterCmd(SYSTICK_COUNTER_CLEAR);
  SYSTICK_CounterCmd(SYSTICK_COUNTER_ENABLE);
}

void MATRIX_setColorAt(uint8_t row, uint8_t col, uint8_t r, uint8_t g, uint8_t b)
{
	gotoXY(row, col);
	setColor(r, g, b);
}

void MATRIX_setpixel(uint16_t x, uint16_t y, uint32_t color)
{
	MATRIX_setColorAt(x, y, 0, 0, 31);
}

void MATRIX_clearScreen(void)
{
	uint8_t row, col;
	for(row = 0; row < MAX_ROW; row++)
	{
		for(col = 0; col < MAX_COL; col++)
		{
			Panel[row][col].r = Panel[row][col].g = Panel[row][col].b = 0;
		}
	}
}

void MATRIX_testFont(void)
{
//	MATRIX_setColorAt(0, 0, 0xAA, 0, 0);
	uint8_t row, col;
	uint16_t ii;
	for(row = 0; row < MAX_ROW; row++)
	{
		for(col = 0; col < MAX_COL; col++)
		{
			ii = MAX_COL*row + col;
			MATRIX_setColorAt(row, col, testFont[ii][0], testFont[ii][1], testFont[ii][2]);
		}
	}
}

void MATRIX_shiftLeft(void)
{
	uint8_t t1, t2, t3;
	uint8_t row, col;
	for(row = 0; row < MAX_ROW; row++)
	{
		t1 = Panel[row][0].r;
		t2 = Panel[row][0].g;
		t3 = Panel[row][0].b;
		for(col = 0; col < MAX_COL-1; col++)
		{
			Panel[row][col].r = Panel[row][col+1].r;
			Panel[row][col].g = Panel[row][col+1].g;
			Panel[row][col].b = Panel[row][col+1].b;
		}	
		Panel[row][MAX_COL-1].r = t1;
		Panel[row][MAX_COL-1].g = t2;
		Panel[row][MAX_COL-1].b = t3;
	}
}

void MATRIX_shiftUp(void)
{
	uint8_t t1, t2, t3;
	uint8_t row, col;
	for(col = 0; col < MAX_COL; col++)
	{
		t1 = Panel[0][col].r;
		t2 = Panel[0][col].g;
		t3 = Panel[0][col].b;
		for(row = 0; row < MAX_ROW-1; row++)
		{

			Panel[row][col].r = Panel[row+1][col].r;
			Panel[row][col].g = Panel[row+1][col].g;
			Panel[row][col].b = Panel[row+1][col].b;
		}	
		Panel[MAX_ROW-1][col].r = t1;
		Panel[MAX_ROW-1][col].g = t2;
		Panel[MAX_ROW-1][col].b = t3;
	}
}

void MATRIX_testPixcel(void)
{
//	MATRIX_setColorAt(0, 0, 0xAA, 0, 0);
	static uint8_t row = 0, col = 0;
	MATRIX_setColorAt(row, col, 0, 31, 0);
	if(++row == MAX_ROW)
	{
		row = 0;
		if(++col == MAX_COL) col = 0;
	}
}
