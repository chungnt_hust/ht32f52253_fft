/*
 * MatrixPxx.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef MATRIXPXX_H
#define MATRIXPXX_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
	
#define MAX_ROW_SCAN 	32 
#define MAX_ROW		 		64
#define MAX_COL		 		128
#define MAX_BIT_POS  	5
typedef struct
{
	uint16_t r:5;
	uint16_t g:5;
	uint16_t b:5;
	uint16_t t:1;
} pixel_t;

typedef struct
{
	uint8_t x;
	uint8_t y;
} point_2D_t;


typedef enum pinNumberLedMatrix
{
	A_PIN = 0,
	B_PIN,
	C_PIN,
	D_PIN,
	E_PIN,
	R1_PIN,
	G1_PIN,
	B1_PIN,
	R2_PIN,
	G2_PIN,
	B2_PIN,
	CLK_PIN,
	LAT_PIN,
	OE_PIN,
	MAX_NUM_PIN_LED_MATRIX
} pinNumber_t;

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void MATRIX_Init(void);
void MATRIX_setColorAt(uint8_t row, uint8_t col, uint8_t r, uint8_t g, uint8_t b);
void MATRIX_setpixel(uint16_t x, uint16_t y, uint32_t color);
void MATRIX_display(uint8_t row);
void MATRIX_enableDisplay(void);
void MATRIX_clearScreen(void);
void MATRIX_testFont(void);
void MATRIX_testPixcel(void);
#endif
#ifdef __cplusplus
}
#endif
