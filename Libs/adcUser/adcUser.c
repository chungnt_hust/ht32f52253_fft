/*
 * adcUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "adcUser.h"
//#include "uartUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define USE_INT 											0
#define USE_INTERNAL_DETECTION				1
#define DEFAULT_VDDA									((float)3.3)

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
int16_t g_vADC;


/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static uint8_t numOfChannel = 0;
static volatile bool adcEOC = FALSE;
//static u32 VR_GPIO_ID[] = {VR0_GPIO_ID, VR1_GPIO_ID, VR2_GPIO_ID, VR3_GPIO_ID,
//													 VR4_GPIO_ID, VR5_GPIO_ID, VR6_GPIO_ID, VR7_GPIO_ID};
//static u32 VR_AFIO_PIN[] = {VR0_AFIO_PIN, VR1_AFIO_PIN, VR2_AFIO_PIN, VR3_AFIO_PIN,
//														VR4_AFIO_PIN, VR5_AFIO_PIN, VR6_AFIO_PIN, VR7_AFIO_PIN};
//static u8 VR_ADC_CH[] = {VR0_ADC_CH, VR1_ADC_CH, VR2_ADC_CH, VR3_ADC_CH,
//												 VR4_ADC_CH, VR5_ADC_CH, VR6_ADC_CH, VR7_ADC_CH};
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  ADC configuration.
  * @retval None
  ***********************************************************************************************************/
void adcUser_DMAConfig(void)
{
	{ /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.PDMA = 1;
		CKCUClock.Bit.AFIO = 1;
    CKCUClock.Bit.ADC0 = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }
		
	{ /* Configure PDMA channel to move ADC result from ADC->DR[0] to TM CHnCCR                               */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below in this function.
    */
    PDMACH_InitTypeDef PDMACH_InitStructure;

    PDMACH_InitStructure.PDMACH_SrcAddr = (u32)&HT_ADC0->DR[0];
    PDMACH_InitStructure.PDMACH_DstAddr = (u32)(&g_vADC);
    PDMACH_InitStructure.PDMACH_BlkCnt = 1;
    PDMACH_InitStructure.PDMACH_BlkLen = 1;
    PDMACH_InitStructure.PDMACH_DataSize = WIDTH_32BIT;
    PDMACH_InitStructure.PDMACH_Priority = VH_PRIO;
    PDMACH_InitStructure.PDMACH_AdrMod = SRC_ADR_FIX | DST_ADR_FIX | AUTO_RELOAD;
    PDMA_Config(PDMA_ADC0, &PDMACH_InitStructure);
    PDMA_EnaCmd(PDMA_ADC0, ENABLE);
		PDMA_IntConfig(PDMA_CH0, PDMA_INT_GE | PDMA_INT_TC, ENABLE);
  }
	
	/* Configure AFIO mode as ADC function                                                                    */
  AFIO_GPxConfig(VR0_GPIO_ID, VR0_AFIO_PIN, AFIO_FUN_ADC0);
	
	{ /* ADC related settings                                                                                 */
		/* CK_ADC frequency is set to (CK_AHB / 64)                                                             */
		CKCU_SetADCnPrescaler(CKCU_ADCPRE_ADC0, CKCU_ADCPRE_DIV64);

		/* Sequence length = 1                                                                   */
		ADC_RegularGroupConfig(HT_ADC0, ONE_SHOT_MODE, 1, 0);

		/* ADC conversion time = (Sampling time + Latency) / CK_ADC = (1.5 + ADST + 12.5) / CK_ADC              */
		/* Set ADST = 0, sampling time = 1.5 + ADST                                                             */
		ADC_SamplingTimeConfig(HT_ADC0, 0);

		/* Set ADC conversion sequence as channel n                                                             */
		ADC_RegularChannelConfig(HT_ADC0, VR0_ADC_CH, 0, 0);
		
		ADC_RegularTrigConfig(HT_ADC0, ADC_TRIG_BFTM1);
	}
		
  /* Issue ADC DMA request when cycle end of conversion occur                                               */
  ADC_PDMAConfig(HT_ADC0, ADC_PDMA_REGULAR_CYCLE, ENABLE);
	NVIC_EnableIRQ(PDMACH0_IRQn);
	
	ADC_Cmd(HT_ADC0, ENABLE);
}

/*********************************************************************************************************//**
  * @brief  ADC configuration.
  * @retval None
  ***********************************************************************************************************/
void adcUser_Config(void)
{
  { /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.AFIO = 1;		// bat bit clock cho khoi ngoai vi
    CKCUClock.Bit.ADC0 = 1;		// bat bit clock ADC
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Configure AFIO mode as ADC                                                          */
	AFIO_GPxConfig(GPIO_PA, AFIO_PIN_0, AFIO_FUN_ADC0);	// cau hinh chan ADC
  

  { /* ADC related settings                                                                                 */
    /* CK_ADC frequency is set to (CK_AHB / 64)                                                             */
    CKCU_SetADCnPrescaler(CKCU_ADCPRE_ADC0, CKCU_ADCPRE_DIV64);

    /* One Shot mode, sequence length = numCH                                                                   */
    ADC_RegularGroupConfig(HT_ADC0, ONE_SHOT_MODE, numOfChannel, 0);	// doc ADC 1 lan khi co start

    /* ADC conversion time = (Sampling time + Latency) / CK_ADC = (1.5 + ADST + 12.5) / CK_ADC              */
    /* Set ADST = 36, sampling time = 1.5 + ADST                                                            */
    ADC_SamplingTimeConfig(HT_ADC0, 36);

    /* Set ADC conversion sequence as channel n                                                             */
		//ADC_RegularChannelConfig(HT_ADC0, ADC_CH_VDD_VREF, 0, 36);	// do VDDA
		
		ADC_RegularChannelConfig(HT_ADC0, ADC_CH_0, 0, 36); // do external ADC PA0
		
		/* Set software trigger as ADC trigger source                                                           */
    ADC_RegularTrigConfig(HT_ADC0, ADC_TRIG_SOFTWARE);	// trigger do ADC bang phan mem
  }

	
	/* Enable ADC     																																												*/							
	ADC_Cmd(HT_ADC0, ENABLE);	// enable ADC
	
#if(USE_INT == 1)
	/* Enable ADC single end of conversion interrupt                                                          */
  ADC_IntConfig(HT_ADC0, ADC_INT_CYCLE_EOC, ENABLE);

  /* Enable the ADC interrupts                                                                              */
  NVIC_EnableIRQ(ADC0_IRQn);
#endif
}

/*********************************************************************************************************//**
 * @brief   This function handles ADC interrupt.
 * @retval  None
 ************************************************************************************************************/
#if(USE_INT == 1)
void ADC_IRQHandler(void)
{
  if(ADC_GetIntStatus(HT_ADC0, ADC_INT_CYCLE_EOC) == SET)
  {
    ADC_ClearIntPendingBit(HT_ADC0, ADC_FLAG_CYCLE_EOC);
		adcEOC = TRUE;
	}
}
#endif
/*********************************************************************************************************//**
  * @brief  ADC read raw data.
  * @retval None
  ***********************************************************************************************************/
void adcUser_ReadData(adcDataType_t *dat)
{
	ADC_SoftwareStartConvCmd(HT_ADC0, ENABLE);
//	DEBUG("ADC start conv\r\n");
//	for(i = 0; i < numOfChannel; i++)
//	{
//		while(ADC_GetFlagStatus(HT_ADC0, ADC_FLAG_SINGLE_EOC) == RESET);
//		ADC_ClearIntPendingBit(HT_ADC0, ADC_FLAG_SINGLE_EOC);
//	}
#if(USE_INT == 1)
	while(adcEOC == FALSE);
	adcEOC = FALSE;
#else
	while(ADC_GetFlagStatus(HT_ADC0, ADC_FLAG_CYCLE_EOC) == RESET);
	ADC_ClearIntPendingBit(HT_ADC0, ADC_FLAG_CYCLE_EOC);
#endif
//	DEBUG("ADC end conv\r\n");
//	for(i = 0; i < numOfChannel; i++) dat->convResult[i] = HT_ADC0->DR[i] & 0x0FFF;

//	dat->vddInV = (DEFAULT_VDDA * 4095) / dat->convResult[0];
	
	dat->convResult[0] = (HT_ADC0->DR[0] & 0x0FFF) - 2047;
}

/*********************************************************************************************************//**
  * @brief  ADC convert data.
  * @retval None
  ***********************************************************************************************************/
void adcUser_Convert(adcDataType_t *dat)
{
//	DEBUG("VDDA =   %fV  \n\r", dat->vddInV);
//	DEBUG("VR0  =   %fV  \n\r", (dat->vddInV * dat->convResult[1]) / dat->convResult[0]);
//	DEBUG("VR1  =   %fV  \n\r", (dat->vddInV * dat->convResult[2]) / dat->convResult[0]);
//	DEBUG("VR2  =   %fV  \n\r", (dat->vddInV * dat->convResult[3]) / dat->convResult[0]);
//	DEBUG("VR3  =   %fV  \n\r", (dat->vddInV * dat->convResult[4]) / dat->convResult[0]);
}
