/*
 * gpioUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 * 
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef GPIOUSER_H
#define GPIOUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define LED_ON 				RESET
#define LED_OFF 			SET

#define CELL_VBAT_ON  SET
#define CELL_VBAT_OFF RESET
#define CELL_PWR_ON  	SET
#define CELL_PWR_OFF 	RESET

#define GNSS_PWR_ON  	SET
#define GNSS_PWR_OFF 	RESET
/***************************  DEBUG_LED  ***************************************//**/
#define DEBUG_LED_GPIOX        	C
#define DEBUG_LED_GPION        	5

#define DEBUG_LED_BIT_CLK      	STRCAT2(P,         				DEBUG_LED_GPIOX)
#define DEBUG_LED_PORTNUM  			STRCAT2(GPIO_P,         	DEBUG_LED_GPIOX)
#define DEBUG_LED_PORT     			STRCAT2(HT_GPIO,        	DEBUG_LED_GPIOX)
#define DEBUG_LED_AFIO_PIN     	STRCAT2(AFIO_PIN_,      	DEBUG_LED_GPION)
#define DEBUG_LED_PIN      			STRCAT2(GPIO_PIN_,      	DEBUG_LED_GPION)	

/***************************  BTN ***************************************//**/
#define BTN_GPIOX       		A
#define BTN_GPION       		2

#define BTN_BIT_CLK     		STRCAT2(P,         				BTN_GPIOX)
#define BTN_PORTNUM  				STRCAT2(GPIO_P,         	BTN_GPIOX)
#define BTN_PORT     				STRCAT2(HT_GPIO,        	BTN_GPIOX)
#define BTN_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	BTN_GPION)
#define BTN_PIN      				STRCAT2(GPIO_PIN_,      	BTN_GPION)

#define BTN_AFIO_EXTI_CH 		STRCAT2(AFIO_EXTI_CH_,  BTN_GPION) 
#define BTN_EXTIO_SOURCE 		STRCAT2(AFIO_ESS_P,     BTN_GPIOX)
#define BTN_EXTI_CHANNEL 		STRCAT2(EXTI_CHANNEL_,  BTN_GPION) 
#define BTN_EXTI_IRQn    		STRCAT3(EXTI, BTN_GPION, _IRQn)
#if(BTN_GPION <= 1)
//	#define BTN_IRQHandler 		EXTI0_1_IRQHandler
#elif(BTN_GPION <= 3)
	#define BTN_IRQHandler 		EXTI2_3_IRQHandler
#else
//	#define BTN_IRQHandler 		EXTI4_15_IRQHandler
#endif
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
extern volatile uint8_t gBtnState;

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports in/out
  * @retval None
  ***********************************************************************************************************/
void Gpio_Config(void);
	
/*********************************************************************************************************//**
  * @brief  Functions for Input mode.
  * @retval None
  ***********************************************************************************************************/
//GPIO_ReadInBit

/*********************************************************************************************************//**
  * @brief  Functions for Output mode.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Toggle_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n);
#endif
#ifdef __cplusplus
}
#endif
