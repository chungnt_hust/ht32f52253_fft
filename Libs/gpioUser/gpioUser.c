/*
 * gpioUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 * All GPIO pins can be selected as EXTI trigger source
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "gpioUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

void (*irqHldFunction)(u16 Data);

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
volatile uint8_t gBtnState = 0;


/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports in/out
  * @retval None
  ***********************************************************************************************************/
void Gpio_Config(void)
{
	/* Configure EXTI Channel n  */
	EXTI_InitTypeDef EXTI_InitStruct;	// struct dung cho cau hinh ngat ngoai 
	/* Configure the GPIO ports. */
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
	
	CKCUClock.Bit.DEBUG_LED_BIT_CLK 		= 1;	// bat clock bit cho chan led
	CKCUClock.Bit.BTN_BIT_CLK 					= 1;	// bat clock bit cho chan btn
	
	CKCUClock.Bit.EXTI         					= 1;	// bact clock bit cho ngoai vi EXT
		
	CKCUClock.Bit.AFIO         					= 1;	// bat bit clock cho khoi ngoai vi
	CKCU_PeripClockConfig(CKCUClock, ENABLE);
	/***************************  DEBUG_LED  ***************************************//**/
	AFIO_GPxConfig(DEBUG_LED_PORTNUM, DEBUG_LED_AFIO_PIN, AFIO_FUN_GPIO);	// chon mode GPIO cho chan led
	GPIO_DirectionConfig(DEBUG_LED_PORT, DEBUG_LED_PIN, GPIO_DIR_OUT);		// chon output
	GPIO_PullResistorConfig(DEBUG_LED_PORT, DEBUG_LED_PIN, GPIO_PR_UP);		// set tro treo len VCC
	GPIO_WriteOutBits(DEBUG_LED_PORT, DEBUG_LED_PIN, LED_OFF);						// xuat gia tri LED_OFF = SET
	/***************************  BTN ***************************************//**/
	AFIO_GPxConfig(BTN_PORTNUM, BTN_AFIO_PIN, AFIO_FUN_GPIO);							// chon mode GPIO cho chan BTN
	GPIO_DirectionConfig(BTN_PORT, BTN_PIN, GPIO_DIR_IN);									// chon input
  GPIO_PullResistorConfig(BTN_PORT, BTN_PIN, GPIO_PR_UP);								// set tro treo len VCC
	GPIO_InputConfig(BTN_PORT, BTN_PIN, ENABLE);													// input enable
#if 1
	/* Select Port as EXTI Trigger Source */
  AFIO_EXTISourceConfig(BTN_AFIO_EXTI_CH, BTN_EXTIO_SOURCE);						// config source cho EXT
	EXTI_InitStruct.EXTI_Channel = BTN_EXTI_CHANNEL;											// ngat channel 2: EXTI_CHANNEL_2
	EXTI_InitStruct.EXTI_Debounce = EXTI_DEBOUNCE_DISABLE;								// khong dung debounce
	EXTI_InitStruct.EXTI_DebounceCnt = 0;																	
	EXTI_InitStruct.EXTI_IntType = EXTI_NEGATIVE_EDGE;										// ngat suong xuong
	EXTI_Init(&EXTI_InitStruct);
	/* Enable EXTI & NVIC line Interrupt */
  EXTI_IntConfig(BTN_EXTI_CHANNEL, ENABLE);															// enable ngat channel 2
  NVIC_EnableIRQ(BTN_EXTI_IRQn);																				// enable ngat
#endif
}

/*********************************************************************************************************//**
  * @brief  Functions for Output mode.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Toggle_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n)
{
  GPIO_WriteOutBits(HT_GPIOx, GPIO_PIN_n, (FlagStatus)!GPIO_ReadOutBit(HT_GPIOx, GPIO_PIN_n));
}

/*********************************************************************************************************//**
 * @brief   This function handles EXTI interrupt.
 * @retval  None
 ************************************************************************************************************/
void BTN_IRQHandler(void)
{
	if(EXTI_GetEdgeFlag(BTN_EXTI_CHANNEL))
  {
    EXTI_ClearEdgeFlag(BTN_EXTI_CHANNEL);
		gBtnState = 1;

  }
}
