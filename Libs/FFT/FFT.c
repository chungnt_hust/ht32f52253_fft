/*
 * sampling : 38KHz
 * frequency FFT 19KHz
 * 64points => buoc fre = 19/64 = 0.296KHz = 296Hz
 * 128points => buoc fre = 19/128 = 0.148KHz = 148Hz
 * 256points => buoc fre = 19/256 = 0.074KHz = 74Hz
 * 512points => buoc fre = 19/512 = 0.037KHz = 37Hz
*/

#include <math.h>
#include "FFT.h"
#include "adcUser.h"
//#include "myHeader.h"

#if (N_point == 16)
#define log2N 4
#elif (N_point == 32)
#define log2N 5
#elif (N_point == 64)
#define log2N 6
#elif (N_point == 128)
#define log2N 7
#elif (N_point == 256)
#define log2N 8
#elif (N_point == 512)
#define log2N 9
#elif (N_point == 1024)
#define log2N 10
#elif (N_point == 2048)
#define log2N 11
#elif (N_point == 4096)
#define log2N 12
#endif

//static TIM_HandleTypeDef *TIMER_SAMPLING = &htim3;
static int16_t xN_data[N_point] = {0};
static uint16_t posCount = 0;
static bool samplingDoneFlag = FALSE;
static bool processFFTDoneFlag = FALSE;
static uint8_t SCALE_FACTOR = (ADC_RESOL - FP_PRECISION);

void FFT_initFreqSampling(uint8_t freq)
{
//	ADC_User_calib();
//	if(freq != FFT_DEFAULT_SAMPLING_FREQ)
//	{
//		TIMER_SAMPLING->Init.Prescaler = 71;
//		TIMER_SAMPLING->Init.CounterMode = TIM_COUNTERMODE_UP;
//		TIMER_SAMPLING->Init.Period = (1000/freq - 1);
//		TIMER_SAMPLING->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//		TIMER_SAMPLING->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
//		HAL_TIM_Base_Init(TIMER_SAMPLING);
//	}
//	HAL_TIM_Base_Start_IT(TIMER_SAMPLING);
	
	{ /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.SCTM0	    	= 1;
    CKCUClock.Bit.AFIO        = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }
		
	{ /* Time base configuration                                                                              */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below this function.
    */
    TM_TimeBaseInitTypeDef TimeBaseInit;

    TimeBaseInit.Prescaler = 1 - 1;            						// Timer clock = CK_AHB / 1
    TimeBaseInit.CounterReload = (40000000/1/38000) - 1;  // CK_AHB/fre
    TimeBaseInit.RepetitionCounter = 0;
    TimeBaseInit.CounterMode = TM_CNT_MODE_UP;
    TimeBaseInit.PSCReloadTime = TM_PSC_RLD_IMMEDIATE;
    TM_TimeBaseInit(HT_SCTM0, &TimeBaseInit);
		/* = 0 Counter reload register can be updated immediately
			 = 1 Counter reload register can not be updated until the update event occurs */
    TM_CRRPreloadCmd(HT_SCTM0, ENABLE);

    /* Clear Update Event Interrupt flag since the "TM_TimeBaseInit()" writes the UEV1G bit                 */
    TM_ClearFlag(HT_SCTM0, TM_FLAG_UEV);
  }
	
	NVIC_EnableIRQ(SCTM0_IRQn);
	TM_IntConfig(HT_SCTM0, TM_INT_UEV, ENABLE);
	TM_Cmd(HT_SCTM0, ENABLE);
}

void FFT_updateXn(uint16_t pos, int16_t dataIn)
{
	xN_data[bitrev_table_ui16[pos]] = dataIn >> SCALE_FACTOR;
}

int16_t I[N_point] = {0};
void FFT_calculate(int16_t *R)
{
	
	uint16_t step, temp3, temp, tem, k, j;
	for(step = 1; step <= log2N; step++)
	{
		temp3 = 1<<(step - 1); 
		for(k = 0; k < temp3; k++)
		{
			temp = 1<<(step);
			tem = k*N_point/temp;
			// int16_t WR = cosTable[tem];
			// int16_t WI = sinTable[tem];		
			int16_t WR = twiddleTableCos[tem];	
			int16_t WI = twiddleTableSin[tem];	
			for(j = 0; j < N_point; j+=temp)
			{						
				int16_t tempR, tempI;
				tempR = FP__MUL(WR, R[j + temp3 + k]) - FP__MUL(WI, I[j + temp3 + k]);
				tempI = FP__MUL(WR, I[j + temp3 + k]) + FP__MUL(WI, R[j + temp3 + k]);
				
				
				R[j + temp3 + k]   = (R[j + k] - tempR);	//WR * R[j + temp3 + k] + WI * I[j + temp3 + k];
				I[j + temp3 + k]   = I[j + k] - tempI;				//WR * I[j + temp3 + k] - WI * temp2;
				
				R[j + k]           = (R[j + k] + tempR);	//WR * R[j + temp3 + k] - WI * I[j + temp3 + k];
				I[j + k]      	   = I[j + k] + tempI;				//WR * I[j + temp3 + k] + WI * temp2;
			}
		}
	}
	
//	for(k = 1; k <= N_freq; k++)
//	{
//		R[k] = sqrt((int16_t)R[k]*(int16_t)R[k] + I[k]*I[k]);
////		R[k] = FP__MUL(R[k], R[k]) + FP__MUL(I[k], I[k]);
//		R[k] >>= 8;
//	}
//	R[10] = (uint16_t)sqrt(FP__MUL(R[10], R[10]) + FP__MUL(I[10], I[10]));
}

adcDataType_t adcData;
/*********************************************************************************************************//**
 * @brief   This function handles SCTM interrupt.
 * @retval  None
 ************************************************************************************************************/
void SCTM0_IRQHandler(void)
{
	
	
	adcUser_ReadData(&adcData);
	FFT_updateXn(posCount++, adcData.convResult[0]);
	if(posCount >= N_point)
	{
		TM_Cmd(HT_SCTM0, DISABLE);
		samplingDoneFlag = TRUE;			
	}
	
	TM_ClearFlag(HT_SCTM0, TM_INT_UEV);
}
//void FFT_TimerInteruptHandle(TIM_HandleTypeDef *htim)
//{
//	if(htim->Instance == TIMER_SAMPLING->Instance)
//	{
//		FFT_updateXn(posCount++, ADC_User_read());
//		if(posCount >= N_point)
//		{
//			HAL_TIM_Base_Stop_IT(TIMER_SAMPLING);
//			samplingDoneFlag = true;			
//		}
//	}
//}
#include "MatrixPxx.h"
void FFT_Process(void)
{
	if(samplingDoneFlag == TRUE)
	{
		samplingDoneFlag = FALSE;
		FFT_calculate(xN_data);
//		if(xN_data[1] > 0)
//			printf("%d\n", xN_data[1]);
		
		
		posCount = 0;
		TM_Cmd(HT_SCTM0, ENABLE);
		processFFTDoneFlag = TRUE;
	}
}
