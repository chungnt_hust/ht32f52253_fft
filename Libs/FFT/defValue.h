/*
 */
 
#ifndef DEFVALUE_H_
#define DEFVALUE_H_

/* define for FFT caculate */
#define PI      		3.1415926535897
#define N_point     	512
#define N_freq       	N_point/2 // frequency N_freq = 0 is DC signal
#define FP_PRECISION 	9 // >= log2N: 1.0 in the floating point <=> 1.0*(1<<FP_PRECISION) in the fix point, 1bit dau + 9bit bieu dien sau dau phay => con 6bit bieu dien so thap phan(max = 64)
#define FP_MASK 		((1<<FP_PRECISION) - 1)
#define FP__MUL(A, B)   ((int16_t)(((int32_t)(A) * (int32_t)(B)) >> FP_PRECISION))
#define ADC_RESOL       12 // 12bit ADC

#endif // DEFVALUE_H_
